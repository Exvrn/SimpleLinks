package me.exvrn.linksPlus.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.exvrn.linksPlus.Main;
import net.md_5.bungee.api.ChatColor;

public class Website implements CommandExecutor {
	private Plugin plugin = Main.getPlugin(Main.class);

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player player = (Player) sender;
		if (sender.hasPermission("links.website")) {
			if (plugin.getConfig().getBoolean("Website.WebsiteEnable")) {

				String Website0 = plugin.getConfig().getString("Website.Line1");
				String Website1 = plugin.getConfig().getString("Website.Line2");
				String Website2 = plugin.getConfig().getString("Website.Line3");
				String Website3 = plugin.getConfig().getString("Website.Line4");
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Website0));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Website1));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Website2));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Website3));

				return true;
			} else {
				String Websiteenable = plugin.getConfig().getString("Website.WebsiteDisableMessage");
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Websiteenable));
				return true;
			}
		}
		return false;
	}

}
