package me.exvrn.linksPlus.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.exvrn.linksPlus.Gui;
import me.exvrn.linksPlus.Main;

public class GuiCommand implements CommandExecutor {
	@SuppressWarnings("unused")
	private Plugin plugin = Main.getPlugin(Main.class);

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player player = (Player) sender;

		if (command.getName().equalsIgnoreCase("links")) {
			if (sender.hasPermission("links.gui")) {
				Gui i = new Gui();
				i.guiInventory(player);
				return true;
			}

		}
		return false;
	}

}
