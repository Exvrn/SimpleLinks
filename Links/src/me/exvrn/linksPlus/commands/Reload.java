package me.exvrn.linksPlus.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.exvrn.linksPlus.Main;
import net.md_5.bungee.api.ChatColor;

public class Reload implements CommandExecutor {
	private Plugin plugin = Main.getPlugin(Main.class);

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player player = (Player) sender;
		String Reload = plugin.getConfig().getString("ReloadMessage");
		if (sender.hasPermission("links.reload")) {
			if (!(sender instanceof Player)) {
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Reload));
				return true;
			}

			plugin.reloadConfig();
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', Reload));

			return true;
		}
		return false;
	}

}
