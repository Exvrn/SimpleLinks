package me.exvrn.linksPlus;

import java.io.File;
import java.util.logging.Logger;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import me.exvrn.linksPlus.commands.Discord;
import me.exvrn.linksPlus.commands.GuiCommand;
import me.exvrn.linksPlus.commands.Reload;
import me.exvrn.linksPlus.commands.TeamSpeak;
import me.exvrn.linksPlus.commands.Twitter;
import me.exvrn.linksPlus.commands.Website;
import me.exvrn.linksPlus.commands.YouTube;
import me.exvrn.linksPlus.events.InvClick;

public class Main extends JavaPlugin {

	public void onEnable() {
		createConfig();
		PluginDescriptionFile pdfFile = getDescription();
		Logger logger = getLogger();
		registerCommands();
		getServer().getPluginManager().registerEvents(new InvClick(), this);
		getServer().getPluginManager().registerEvents(new Gui(), this);

		logger.info(pdfFile.getName() + " Has been enabled (V. " + pdfFile.getVersion() + ")");

	}

	private void createConfig() {
		try {
			if (!getDataFolder().exists()) {
				getDataFolder().mkdirs();
			}
			File file = new File(getDataFolder(), "config.yml");
			if (!file.exists()) {
				getLogger().info("Config.yml not found, creating!");
				saveDefaultConfig();
			} else {
				getLogger().info("Config.yml found, loading!");
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	public void onDisable() {
		PluginDescriptionFile pdfFile = getDescription();
		Logger logger = getLogger();

		logger.info(pdfFile.getName() + " Has been disabled (V. " + pdfFile.getVersion() + ")");
	}

	public void registerCommands() {
		getCommand("discord").setExecutor(new Discord());
		getCommand("youtube").setExecutor(new YouTube());
		getCommand("website").setExecutor(new Website());
		getCommand("linksreload").setExecutor(new Reload());
		getCommand("teamspeak").setExecutor(new TeamSpeak());
		getCommand("twitter").setExecutor(new Twitter());
		getCommand("links").setExecutor(new GuiCommand());

	}

}
