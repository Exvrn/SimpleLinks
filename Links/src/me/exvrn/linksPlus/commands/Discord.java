package me.exvrn.linksPlus.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.exvrn.linksPlus.Main;
import net.md_5.bungee.api.ChatColor;

public class Discord implements CommandExecutor {
	private Plugin plugin = Main.getPlugin(Main.class);

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player player = (Player) sender;
		if (sender.hasPermission("links.discord")) {
			if (plugin.getConfig().getBoolean("Discord.DiscordEnable")) {

				String Discord0 = plugin.getConfig().getString("Discord.Line1");
				String Discord1 = plugin.getConfig().getString("Discord.Line2");
				String Discord2 = plugin.getConfig().getString("Discord.Line3");
				String Discord3 = plugin.getConfig().getString("Discord.Line4");
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Discord0));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Discord1));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Discord2));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Discord3));

				return true;
			} else {
				String Discordenable = plugin.getConfig().getString("Discord.DiscordDisableMessage");
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Discordenable));
				return true;
			}
		}

		return false;
	}

}
