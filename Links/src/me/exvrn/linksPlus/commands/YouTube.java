package me.exvrn.linksPlus.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.exvrn.linksPlus.Main;
import net.md_5.bungee.api.ChatColor;

public class YouTube implements CommandExecutor {
	private Plugin plugin = Main.getPlugin(Main.class);

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player player = (Player) sender;
		if (sender.hasPermission("links.youtube")) {
			if (plugin.getConfig().getBoolean("Website.WebsiteEnable")) {

				String YouTube0 = plugin.getConfig().getString("YouTube.Line1");
				String YouTube1 = plugin.getConfig().getString("YouTube.Line2");
				String YouTube2 = plugin.getConfig().getString("YouTube.Line3");
				String YouTube3 = plugin.getConfig().getString("YouTube.Line4");
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', YouTube0));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', YouTube1));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', YouTube2));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', YouTube3));

				return true;
			} else {
				String YouTubeenable = plugin.getConfig().getString("YouTube.YouTubeDisableMessage");
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', YouTubeenable));
				return true;
			}
		}
		return false;
	}

}
