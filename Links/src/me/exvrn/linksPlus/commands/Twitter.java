package me.exvrn.linksPlus.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.exvrn.linksPlus.Main;
import net.md_5.bungee.api.ChatColor;

public class Twitter implements CommandExecutor {
	private Plugin plugin = Main.getPlugin(Main.class);

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player player = (Player) sender;
		if (sender.hasPermission("links.twitter")) {
			if (plugin.getConfig().getBoolean("Twitter.TwitterEnable")) {

				String Twitter0 = plugin.getConfig().getString("Twitter.Line1");
				String Twitter1 = plugin.getConfig().getString("Twitter.Line2");
				String Twitter2 = plugin.getConfig().getString("Twitter.Line3");
				String Twitter3 = plugin.getConfig().getString("Twitter.Line4");
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Twitter0));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Twitter1));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Twitter2));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Twitter3));

				return true;
			} else {
				String Twitterenable = plugin.getConfig().getString("Twitter.TwitterDisableMessage");
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', Twitterenable));
				return true;
			}
		}
		return false;
	}

}
