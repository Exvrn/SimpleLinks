package me.exvrn.linksPlus.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.exvrn.linksPlus.Main;
import net.md_5.bungee.api.ChatColor;

public class TeamSpeak implements CommandExecutor {
	private Plugin plugin = Main.getPlugin(Main.class);

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player player = (Player) sender;
		if (sender.hasPermission("links.teamspeak")) {
			if (plugin.getConfig().getBoolean("TeamSpeak.TeamSpeakEnable")) {

				String TeamSpeak0 = plugin.getConfig().getString("TeamSpeak.Line1");
				String TeamSpeak1 = plugin.getConfig().getString("TeamSpeak.Line2");
				String TeamSpeak2 = plugin.getConfig().getString("TeamSpeak.Line3");
				String TeamSpeak3 = plugin.getConfig().getString("TeamSpeak.Line4");
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', TeamSpeak0));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', TeamSpeak1));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', TeamSpeak2));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', TeamSpeak3));

				return true;

			} else {
				String TeamSpeakenable = plugin.getConfig().getString("TeamSpeak.TeamSpeakDisableMessage");
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', TeamSpeakenable));
				return true;

			}
		}
		return false;
	}

}
