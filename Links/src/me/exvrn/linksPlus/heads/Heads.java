package me.exvrn.linksPlus.heads;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import net.minecraft.server.v1_12_R1.NBTTagCompound;
import net.minecraft.server.v1_12_R1.NBTTagList;
import net.minecraft.server.v1_12_R1.NBTTagString;

public class Heads {

	org.bukkit.inventory.ItemStack HeadItem = new org.bukkit.inventory.ItemStack(Material.SKULL_ITEM, 1, (short) 3);

	public Heads(String playername) {
		org.bukkit.inventory.ItemStack itemStack = new org.bukkit.inventory.ItemStack(Material.SKULL_ITEM, 1,
				(short) 3);
		SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();

		skullMeta.setOwner(playername);
		skullMeta.setDisplayName("Head of " + playername);
		itemStack.setItemMeta(skullMeta);

		this.HeadItem = itemStack;
	}

	public Heads(String skullname, String id, String textureValue) {
		org.bukkit.inventory.ItemStack itemStack = new org.bukkit.inventory.ItemStack(Material.SKULL_ITEM, 1,
				(short) 3);

		ItemMeta PlusMeta = itemStack.getItemMeta();
		PlusMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', skullname));
		itemStack.setItemMeta(PlusMeta);

		net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(itemStack);

		NBTTagCompound compound = nmsStack.getTag();
		if (compound == null) {
			compound = new NBTTagCompound();
			nmsStack.setTag(compound);
			compound = nmsStack.getTag();
		}
		NBTTagCompound skullOwner = new NBTTagCompound();
		skullOwner.set("Id", new NBTTagString(id));
		NBTTagCompound properties = new NBTTagCompound();
		NBTTagList textures = new NBTTagList();
		NBTTagCompound value = new NBTTagCompound();
		value.set("Value", new NBTTagString(textureValue));
		textures.add(value);
		properties.set("textures", textures);
		skullOwner.set("Properties", properties);

		compound.set("SkullOwner", skullOwner);
		nmsStack.setTag(compound);

		this.HeadItem = CraftItemStack.asBukkitCopy(nmsStack);
	}

	public void giveTo(Player player) {
		player.getInventory().addItem(new org.bukkit.inventory.ItemStack[] { this.HeadItem });
	}

	public org.bukkit.inventory.ItemStack getItemStack() {
		return this.HeadItem;
	}
}
