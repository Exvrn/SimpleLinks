package me.exvrn.linksPlus;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import me.exvrn.linksPlus.heads.Heads;
import net.md_5.bungee.api.ChatColor;

public class Gui implements Listener {
	private Plugin plugin = Main.getPlugin(Main.class);

	public void guiInventory(Player player) {
		String guiName = plugin.getConfig().getString("GuiTitle");
		Inventory i = plugin.getServer().createInventory(null, 27, ChatColor.translateAlternateColorCodes('&', guiName));

		String twittergui = plugin.getConfig().getString("TwitterGuiName");
		String youtubegui = plugin.getConfig().getString("YoutubeGuiName");
		String teamspeakgui = plugin.getConfig().getString("TeamSpeakGuiName");
		String discordgui = plugin.getConfig().getString("DiscordGuiName");
		String websitegui = plugin.getConfig().getString("WebsiteGuiName");

		Heads Twitter = new Heads(twittergui, "11fce6c7-71ad-464e-98e7-c8e579de4758",
				"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzY4NWEwYmU3NDNlOTA2N2RlOTVjZDhjNmQxYmEyMWFiMjFkMzczNzFiM2Q1OTcyMTFiYjc1ZTQzMjc5In19fQ==");
		Heads Discord = new Heads(discordgui, "de431cd1-ae1d-49f6-9339-a96daeacc32b",
				"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzg3M2MxMmJmZmI1MjUxYTBiODhkNWFlNzVjNzI0N2NiMzlhNzVmZjFhODFjYmU0YzhhMzliMzExZGRlZGEifX19");
		Heads Website = new Heads(websitegui, "dd772978-53f5-4130-a232-a204173445b6",
				"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWIyMTQxNWYxY2U2ZGI2NzdjYWQzOWMzM2I1N2Q2OGRkODM1NmM3MWJlNTVjYmM1ZTg3YTM2YjYyMmE4NDFmIn19fQ==");
		Heads teamspeak = new Heads(teamspeakgui, "5c612ffe-4461-4b21-bb0e-3fcd110bd6a6",
				"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzQ1MTg5N2Q3Zjc0N2E5MDFjMTgzYmZlZTJlZDE3NGYzNTY1NWM5NjZmOWFkZjZlMmM3NjMwYTAzYTgxNTViZCJ9fX0=");
		Heads youtube = new Heads(youtubegui, "6486882a-2871-4db3-922d-98ebc3166c6b",
				"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZDJmNmMwN2EzMjZkZWY5ODRlNzJmNzcyZWQ2NDU0NDlmNWVjOTZjNmNhMjU2NDk5YjVkMmI4NGE4ZGNlIn19fQ==");
		i.setItem(13, Twitter.getItemStack()); // twitter
		i.setItem(9, teamspeak.getItemStack());
		i.setItem(11, youtube.getItemStack());
		i.setItem(15, Discord.getItemStack());
		i.setItem(17, Website.getItemStack());

		ItemStack empty = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 15);
		ItemMeta emptyMeta = empty.getItemMeta();

		emptyMeta.setDisplayName(" ");
		List<String> emptyLore = null;
		emptyMeta.setLore(emptyLore);
		empty.setItemMeta(emptyMeta);

		// Empty Slots :
		i.setItem(0, empty); // empty
		i.setItem(1, empty); // empty
		i.setItem(2, empty); // empty
		i.setItem(3, empty); // empty
		i.setItem(4, empty); // empty
		i.setItem(5, empty); // empty
		i.setItem(3, empty); // empty
		i.setItem(5, empty); // empty
		i.setItem(6, empty); // empty
		i.setItem(7, empty); // empty
		i.setItem(8, empty); // empty
		i.setItem(10, empty); // empty
		i.setItem(12, empty); // empty
		i.setItem(14, empty); // empty
		i.setItem(16, empty); // empty
		i.setItem(18, empty); // empty
		i.setItem(19, empty); // empty
		i.setItem(20, empty); // empty
		i.setItem(21, empty); // empty
		i.setItem(22, empty); // empty
		i.setItem(23, empty); // empty
		i.setItem(24, empty); // empty
		i.setItem(25, empty); // empty
		i.setItem(26, empty); // empty
		

		player.openInventory(i);
	}

}
