package me.exvrn.linksPlus.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import me.exvrn.linksPlus.Gui;
import me.exvrn.linksPlus.Main;
import net.md_5.bungee.api.ChatColor;

public class InvClick implements Listener {
	private Plugin plugin = Main.getPlugin(Main.class);

	@EventHandler
	public void InventoryClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		String guiName2 = plugin.getConfig().getString("GuiTitle");
		Inventory open = e.getClickedInventory();
		ItemStack item = e.getCurrentItem();
		if (open == null) {
			e.setCancelled(true);
			return;
		}
		if (open.getName().equals(ChatColor.translateAlternateColorCodes('&', guiName2))) {
			if (item.equals(null) || !item.hasItemMeta()) {
				e.setCancelled(true);
				return;
			}
			String twittergui = plugin.getConfig().getString("TwitterGuiName");
			if (item.getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes('&', twittergui))) {
				Gui gi = new Gui();
				gi.guiInventory(player);
				player.closeInventory();

				Bukkit.dispatchCommand(player, "twitter");

			}
			String youtubegui = plugin.getConfig().getString("YoutubeGuiName");
			if (item.getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes('&', youtubegui))) {
				Gui gi = new Gui();
				gi.guiInventory(player);
				player.closeInventory();

				Bukkit.dispatchCommand(player, "youtube");

			}
			String teamspeakgui = plugin.getConfig().getString("TeamSpeakGuiName");
			if (item.getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes('&', teamspeakgui))) {
				Gui gi = new Gui();
				gi.guiInventory(player);
				player.closeInventory();
				Bukkit.dispatchCommand(player, "teamspeak");

			}
			String discordgui = plugin.getConfig().getString("DiscordGuiName");
			if (item.getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes('&', discordgui))) {
				Gui gi = new Gui();
				gi.guiInventory(player);
				player.closeInventory();
				player.closeInventory();

				Bukkit.dispatchCommand(player, "discord");

			}
			String websitegui = plugin.getConfig().getString("WebsiteGuiName");
			if (item.getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes('&', websitegui))) {
				Gui gi = new Gui();
				gi.guiInventory(player);
				player.closeInventory();
				player.closeInventory();

				Bukkit.dispatchCommand(player, "website");

			}
			if (item.getItemMeta().getDisplayName().equals(" ")) {

				e.setCancelled(true);
			}

		}
	}

}
